#!/usr/bin/python3

from sys import argv



def suma(m, n):
    return m + n


def resta(m, n):
    if m > n:
        return m - n
    else:
        return n - m


if __name__ == "__main__":
    try:
        if argv[1] == "suma":
            sum = suma(int(argv[2]), int(argv[3]))
            print(str(argv[2]) + " mas " + str(argv[3]) + " es: " + str(sum))
        elif argv[1] == "resta":
            rest = resta(int(argv[2]), int(argv[3]))
            print(str(argv[2]) + " menos " + str(argv[3]) + " es: " + str(rest))
    except ValueError:
        print("Por favor, escriba de manera correcta los argumentos")
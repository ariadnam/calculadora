def suma(m, n):
    return m + n


def resta(m, n):
    if m > n:
        return m - n
    else:
        return n - m


if __name__ == "__main__":
    for i in range(1, 4):
        sum = suma(i, (i + 1))
        print(str(i) + " mas " + str(i + 1) + " es: " + str(sum))
    for j in range(5, 8):
        rest = resta(j, (j + 1))
        print(str(j) + " menos " + str(j + 1) + " es: " + str(rest))